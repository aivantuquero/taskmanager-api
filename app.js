const mysql = require("mysql2/promise");
const cors = require("cors");
const express = require("express");

// Create a connection to the database
async function initializeDatabase() {
  return (connection = await mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "taskmanager_db",
  }));
}

let connection = initializeDatabase();
const app = express();
const port = 3000;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// fetches all tasks from the database
app.get("/", async (req, res) => {
  try {
    const [results] = await connection.query("SELECT * FROM `tasks`");
    res.send(results);
  } catch (err) {
    console.log(err);
  }
});

// adds a single task from the database
app.post("/add-task", async (req, res) => {
  try {
    const { title } = req.body;
    const { status } = req.body;
    const [results] = await connection.query(
      "INSERT INTO `tasks` (`title`, `status`) VALUES (?, ?)",
      [title, status]
    );

    res.send(results);
  } catch (err) {
    console.log(err);
  }
});

// deletes a single task from the database
app.delete("/delete-task", async (req, res) => {
  try {
    const { id } = req.body;
    const [results] = await connection.query(
      "DELETE FROM `tasks` WHERE `id` = ?",
      [id]
    );

    res.send(results);
  } catch (err) {
    console.log(err);
  }
});

// edits a single task from the database
app.put("/update-task", async (req, res) => {
  try {
    const { id } = req.body;
    const { status } = req.body;
    const [results] = await connection.query(
      "UPDATE `tasks` SET `status` = ? WHERE `id` = ?",
      [status, id]
    );

    res.send(results);
  } catch (err) {
    console.log(err);
  }
});

app.listen(port, () => {
  console.log(`TaskManager app listening on port ${port}`);
});
